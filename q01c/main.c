#include <dlfcn.h>
#include <stdio.h>

int main(){

  void *handle;
  char *error;
  int (*primo)();
  int n;

  handle = dlopen("./libprimo.so", RTLD_LAZY);

  if(!handle){
    printf("%s\n", dlerror());
    return 1;
  }

  primo = dlsym(handle, "gera_primo");
  if((error = dlerror()) != 0){
    printf("%d\n", *error);
    return 1;
  }

  n = (*primo)();
  printf("\nNumero Primo Gerado = %d\n\n", n);

  primo = dlsym(handle, "testa_primo");
  if((error = dlerror()) != 0){
    printf("%d\n", *error);
    return 1;
  }

  if((*primo)(n))
    printf("O numero %d e primo.\n\n", n);
  else
    printf("O numero %d nao e primo.\n\n", n);

  dlclose(handle);

  return 0;
}
