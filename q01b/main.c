#include "libprimo.h"
#include <stdio.h>

int main(){

  int i, n;

  n = gera_primo();
  printf("\nNumero Primo Gerado = %d\n\n", n);
  if(testa_primo(n))
    printf("O numero %d e primo.\n\n", n);
  else
    printf("O numero %d nao e primo.\n\n", n);

  return 0;
}
